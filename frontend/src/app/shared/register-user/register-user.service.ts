import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { UserExists } from "../../models/user-exists";
import { Observable } from "rxjs/Observable";
@Injectable()
export class RegisterUserService {
  private registerUser: BehaviorSubject<UserExists | null> = new BehaviorSubject(null);
private static readonly STORAGE_KEY = "acs_register";

  constructor() {
    this.retrieveUserFromStorage();
  }

  public setRegisteredUser(user: UserExists): void {
    this.registerUser.next(user||null);
    this.saveUserToStorage();
  }

  public getRegisteredUser(): UserExists | null {
    return this.registerUser.getValue();
  }

  public isRegisteredUser(): boolean {
    return !!this.registerUser.getValue();
  }


  private saveUserToStorage(): void {
    sessionStorage.setItem(RegisterUserService.STORAGE_KEY, JSON.stringify(this.getRegisteredUser()));
  }

  private retrieveUserFromStorage(): void {
    const rawStorageUser = sessionStorage.getItem(RegisterUserService.STORAGE_KEY);

    try {
      let storageUser: UserExists = JSON.parse(rawStorageUser);
      this.setRegisteredUser(storageUser);

    } catch (e) {
      console.error(`Cannot parse registered user from storage: ${rawStorageUser}`);
    }
  }
}
