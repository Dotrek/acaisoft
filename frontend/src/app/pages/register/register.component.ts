import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { AcaisoftHttpService } from "../../shared/http/acaisoft-http.service";
import {RegisterUserService} from "../../shared/register-user/register-user.service"


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  formGroup: FormGroup;
  constructor(
    private http : AcaisoftHttpService,
    private router: Router,
    private registerUserService : RegisterUserService
  ) {}

  ngOnInit() {
    this.formGroup = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", Validators.required),
      confirmPassword:new FormControl("", Validators.required)
    });
  }
  register() {
    if (this.formGroup.invalid) {
      this.markControls(this.formGroup, control => control.markAsDirty());
      console.warn("Cannot register, formGroup is invalid!");
      return;
    }
  }
  private markControls(form: FormGroup, callback: (control: AbstractControl) => void): void {
    Object.keys(form.controls)
      .forEach(key => {
        callback(form.get(key));
      })}
}

