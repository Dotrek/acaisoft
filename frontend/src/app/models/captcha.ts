export interface Captcha {
  formId: string,
  textValue: string
}
