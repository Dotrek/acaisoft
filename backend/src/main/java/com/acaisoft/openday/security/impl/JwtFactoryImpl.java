package com.acaisoft.openday.security.impl;


import com.acaisoft.openday.security.Jwt;
import com.acaisoft.openday.security.JwtFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class JwtFactoryImpl implements JwtFactory{
    private String jwtKey;

    @Autowired
    public JwtFactoryImpl(@Value("${app.jwt.signing.key}") String jwtKey) {
        this.jwtKey = jwtKey;
    }

    @Override
    public Jwt jwt() {
        return new JwtImpl(this.jwtKey);
    }
}
