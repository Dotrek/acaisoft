package com.acaisoft.openday.schema;

import java.util.List;

public class User {
    private String username;
    private String password;
    private List<Book> borrowedBooks;


    public void setBorrowedBooks(List<Book> borrowedBooks) {
        this.borrowedBooks = borrowedBooks;
    }

    public List<Book> getBorrowedBooks() {
        return borrowedBooks;
    }

    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public User addBorrowedBook(Book book) {
        this.borrowedBooks.add(book);
        return this;
    }

    public User returnBook(Book book) {
        this.borrowedBooks.remove(book);
        return this;
    }
}
