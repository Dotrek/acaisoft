package com.acaisoft.openday.views.user.dto;

import javax.validation.constraints.NotNull;

public class SimpleRegisterUserDto {
    @NotNull
    private String email;
    @NotNull
    private String password;

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
