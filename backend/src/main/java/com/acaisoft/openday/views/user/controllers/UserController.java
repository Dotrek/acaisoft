package com.acaisoft.openday.views.user.controllers;

import com.acaisoft.openday.captcha.CaptchaManager;
import com.acaisoft.openday.db.CaptchaDB;
import com.acaisoft.openday.db.CaptchaDBException;
import com.acaisoft.openday.db.UsersDB;
import com.acaisoft.openday.db.UsersDBException;
import com.acaisoft.openday.schema.Captcha;
import com.acaisoft.openday.schema.User;
import com.acaisoft.openday.InternalServerException;
import com.acaisoft.openday.views.user.dto.*;
import com.acaisoft.openday.views.user.exceptions.UserAuthorizationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class UserController {

    private UsersDB usersDB;
    private CaptchaDB captchaDB;
    private CaptchaManager captchaManager;

    @Autowired
    public UserController(UsersDB usersDB, CaptchaDB captchaDB, CaptchaManager captchaManager) {
        this.usersDB = usersDB;
        this.captchaDB = captchaDB;
        this.captchaManager = captchaManager;
    }


    @PostMapping("/user/register/simple")
    public ResponseEntity registerSimple(@RequestBody @Valid SimpleRegisterUserDto userData) throws UserAuthorizationException, UsersDBException {
        this.usersDB.registerUser(userData.getEmail(), userData.getPassword());

        return ResponseEntity.ok().build();
    }

    @PostMapping("/user/register/captcha")
    public ResponseEntity registerCaptcha(@RequestBody @Valid CaptchaRegisterUserDto userData) throws UserAuthorizationException, CaptchaDBException, UsersDBException {
        boolean captchaValid = this.captchaDB.isCaptchaValid(userData.getCaptcha().getFormId(), userData.getCaptcha().getTextValue());
        if (!captchaValid) {
            throw new UserAuthorizationException("Invalid captcha");
        }

        boolean areCredentialsValid = this.usersDB.areCredentialsValid(userData.getEmail(), userData.getPassword());
        if (!areCredentialsValid) {
            throw new UserAuthorizationException("Invalid credentials");
        }

        this.usersDB.registerUser(userData.getEmail(), userData.getPassword());
        this.captchaDB.invalidateCaptcha(userData.getCaptcha().getFormId());
        return ResponseEntity.ok().build();

    }

    @GetMapping("/user/register/captcha/generate")
    public ResponseEntity<CaptchaDto> generateCaptcha() throws InternalServerException {
        Captcha captcha = this.captchaManager.generateCaptcha();

        try {
            this.captchaDB.saveCaptcha(captcha);
        } catch (CaptchaDBException e) {
            throw new InternalServerException("Error while saving captcha to DB. Reason: " + e.getMessage());
        }

        return ResponseEntity.ok(captcha.toDto());
    }

    @GetMapping("/user/exists/{username}")
    public ResponseEntity<UserExistsDto> doesUserExist(@PathVariable String username) {
        Optional<User> userOpt = this.usersDB.getUser(username);

        UserExistsDto response = new UserExistsDto().setUserExists(userOpt.isPresent());

        return ResponseEntity.ok(response);
    }

}
