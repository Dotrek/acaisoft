package com.acaisoft.openday.views.user.dto;

import javax.validation.constraints.NotNull;

public class CaptchaDto {
    @NotNull
    private String formId;
    @NotNull
    private String textValue;

    public String getFormId() {
        return formId;
    }

    public CaptchaDto setFormId(String formId) {
        this.formId = formId;
        return this;
    }

    public String getTextValue() {
        return textValue;
    }

    public CaptchaDto setTextValue(String textValue) {
        this.textValue = textValue;
        return this;
    }
}
